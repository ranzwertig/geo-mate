var io = require('socket.io');
var app = require('express')();
var express = require('express');
var server = require('http').createServer(app);
var consolidate = require('consolidate');

 
app.configure(function () {
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session({secret: 'secret', key: 'express.sid'}));
    app.use("/assets", express.static(__dirname + '/assets'));
    app.set("views", __dirname+'/html');
    app.engine('html', consolidate.mustache);
    app.set('view engine', 'html');
});

app.get('/', function(request, response){
    response.render('index.html');
});

app.get('/:tag', function(request, response){
    var tag = request.params.tag;

    response.render('tag.html', {
        tag: tag
    });
});

port = process.env.PORT || 80;

server.listen(port, function(){
    console.log("Listening on " + port);
});

var sio = io.listen(server);
 
var geoData = {};
var idConnections = {};

sio.sockets.on('connection', function (socket) {


    socket.on('hello', function(data){

        if (typeof idConnections[data.tag] === 'undefined') {
            idConnections[data.tag] = {};
        }

        idConnections[data.tag][data.id] = socket;

        for(var coordKey in geoData[data.tag]) {
            var coord = geoData[data.tag][coordKey];
            socket.emit('view', coord);
        }

    });

    socket.on('hb', function (data) {
        console.log('works');
        socket.broadcast.emit('br', data);
    });

    socket.on('position', function (data) {

        console.log(data);

        if (typeof geoData[data.tag] === 'undefined') {
            geoData[data.tag] = {};
        }

        geoData[data.tag][data.id] = data;

        for (var connKey in idConnections[data.tag]) {
            var s = idConnections[data.tag][connKey];
            s.emit('view', geoData[data.tag][data.id]);
        }

    });
});