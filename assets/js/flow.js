$(document).ready(function(){

  var height = parseInt($(window).height(), 10) - 40;
  var width = parseInt($(window).width(),10);

  var oHeight = parseInt($(window).height(), 10) - 60;
  var oWidth = parseInt($(window).width(),10) - 20;

  $('.view').css('height', height + 'px');
  $('.view').css('width',  width + 'px');

  $('.overlay').css('height', oHeight + 'px');
  $('.overlay').css('width',  oWidth + 'px');

  // handle window resize
  $(window).resize(function(){
    var height = parseInt($(window).height(), 10) - 40;
    var width = parseInt($(window).width(),10);
    $('.view').css('height', height + 'px');
    $('.view').css('width',  width + 'px');

    var oHeight = parseInt($(window).height(), 10) - 60;
    var oWidth = parseInt($(window).width(),10) - 20;
    $('.overlay').css('height', oHeight + 'px');
    $('.overlay').css('width',  oWidth + 'px');
  });

  // restore view after reload
  if($.cookie('lastView')) {
    $('.view').hide();
    var view = $.cookie('lastView');
    $('#'+view).show();
    $('.btn.active').removeClass('active');
    $('.btn[data-view='+view+']').addClass('active');
  }

  // select view
  $('.btn').click(function(){
    $('.view').hide();
    var view = $(this).attr('data-view');
    $('#'+view).show();
    $('.btn.active').removeClass('active');
    $(this).addClass('active');
    $.cookie('lastView', view);
  });

  $('#bMyPos').click(function(){
    $(this).toggleClass('on');
    map.setCenter(currentPosition.lat, currentPosition.lng);
    mapCentered = true;
  });

  $('.overlay-close').click(function(){
    $('.obtn').parent().parent().hide();
  });

});