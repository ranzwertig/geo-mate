var UPDATE_INTERVAL = 3000;

var watchIntervall = null;
var currentPosition = null;
var mapCentered = true;
var initCenter = true;
var lastPositionChange = 0;

var browserId = null;
var map = null;

$(document).ready(function(){

  if ($.cookie('browserId')) {
    browserId = $.cookie('browserId');
  } else {
    browserId = (new Date()).getTime() + '-' + navigator.userAgent;
    $.cookie('browserId', browserId, { expires: 7 });
  }

  var lastLat = 49.0000;
  var lastLng = 9.89900;
  if ($.cookie('lastLat') && $.cookie('lastLng')) {
    lastLat = $.cookie('lastLat');
    lastLng = $.cookie('lastLng');
  }

  map = new GMaps({
      el: '#map',
      lat: lastLat,
      lng: lastLng,
      zoom: 13,
      markerClusterer: function(map) {
        return new MarkerClusterer(map, [], {
          zoomOnClick: false,
          clusterClick: function(e) {
            var clusterSize = e.markers_.length;
            $('#mapOverlayHeadline').html(clusterSize + ' Users in Area');
            $('#mapOverlay').show();
          }
        });
      },
      dragend: function(e){
        if ($('#bMyPos').hasClass('on')){
          $('#bMyPos').removeClass('on');
        }
        mapCentered = false;
      },
      zoom_changed: function(){
        if ($('#bMyPos').hasClass('on')){
          $('#bMyPos').removeClass('on');
        }
        mapCentered = false;
      }
  });


  // open socket and weit for status
  var socket = io.connect('http://geo.brl.io/');
  socket.on('connect', function(){

    //set status views
    $('#statusb').addClass('icon-ok-circle');
    $('#statusb').removeClass('icon-ban-circle');
    $('#connection').removeClass('error');
    $('#connection').addClass('ok');

    // register the ap at service by sending the tag and the browser id
    socket.emit('hello', {
        tag: $('#tag').val(),
        id: browserId
    });

  });

  // listen on disconnect
  socket.on('disconnect', function(){

    // set status views
    $('#statusb').removeClass('icon-ok-circle');
    $('#statusb').addClass('icon-ban-circle');
    $('#connection').removeClass('ok');
    $('#connection').addClass('error');

  });
  
  // markers storage holdign all reveived markers
  var markers = {};

  // function to draw the marker to the map
  var drawMarker = function(marker){
    map.addMarker(marker);
  };

  // getting a geo data from the server on view channel
  socket.on('view', function (data) {
    console.log('recieve position');
    var details = {
      tag: data.tag,
      id: data.id,
      date: data.date,
      accuracy: data.accuracy,
      name: data.name
    };
    if (typeof markers[data.id] === 'undefined') {
      console.log('new position');
      markers[data.id] = map.createMarker({
        position: new google.maps.LatLng(data.position.lat, data.position.lng),
        title: JSON.stringify(details),
        click: function(e){
          var info = JSON.parse(e.title);
          if (typeof info.name === 'undefined' || info.name === '') {
            $('#mapOverlayHeadline').html('Anonymous');
          } else {
            $('#mapOverlayHeadline').html(info.name);
          }
          $('#mapOverlay').show();
        }
      });

      drawMarker(markers[data.id]);
    } else {
      console.log('update position');
      var marker = markers[data.id];
      marker.setPosition(new google.maps.LatLng(data.position.lat, data.position.lng));
      marker.setTitle(JSON.stringify(details));
    }
  });


  socket.on('connect', function(){
    console.log('established socked connection');
    document.getElementById('status').innerHTML = 'connected';
  });

  socket.on('br', function(data){
    //alert('bt');
  });

  // if geolocation is supported
  if (navigator.geolocation) {
    // first get position to set the map center
    var getLocation = function(position){
      console.log(position);
      
      lastPositionChange = (new Date()).getTime();
      lastLat = $.cookie('lastLat', position.coords.latitude, { expires: 7 });
      lastLng = $.cookie('lastLng', position.coords.longitude, { expires: 7 });
      currentPosition = {
        lng: position.coords.longitude,
        lat: position.coords.latitude
      };
      if (initCenter){
        map.setCenter(position.coords.latitude, position.coords.longitude);
        if (mapCentered) {
          initCenter = true;
        } else {
          initCenter = false;
        }
      }
      // send the current position and data to server
      console.log('emit position');
      socket.emit('position', {
        position: {
          lng: position.coords.longitude,
          lat: position.coords.latitude
        },
        date: position.timestamp,
        accuracy: position.coords.accuracy,
        tag: document.getElementById('tag').value,
        name: document.getElementById('name').value,
        id: browserId
      }, function(error){
        console.log('position broadcasted');
        if (error) {
          console.log(error);
        }
      });
    };

    /*watchIntervall = navigator.geolocation.watchPosition(getLocation, function(error){
      document.getElementById('status').innerHTML = error.message;
      console.log(error);
    });*/

    setInterval(function(){
      console.log('hearbreat intervall');
      if ((new Date()).getTime() - lastPositionChange >= 5000) {
        socket.emit('hb', {});
        console.log('sending position manually');
        navigator.geolocation.getCurrentPosition(getLocation, function(error){
          document.getElementById('status').innerHTML = error.message;
          console.log(error);
        });
      }
    }, UPDATE_INTERVAL);
  } else {
      // geolocation error
      document.getElementById('status').innerHTML = 'geolocation not available';
  }

  // socket gor disconnected
  socket.on('disconnect', function(){
    console.log('socket disconnected');
    if (watchIntervall !== null) {
      navigator.geolocation.clearWatch(watchIntervall);
    }
    document.getElementById('status').innerHTML = 'disconnected';
  });

});
