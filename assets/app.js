$(document).ready(function(){

    var map = new GMaps({
        el: '#map',
        lat: -12.043333,
        lng: -77.028333
    });


    var watchId = null;
    var browserId = null;
    if ($.cookie('browserId')) {
      browserId = $.cookie('browserId');
    } else {
      browserId = (new Date()).getTime() + '-' + navigator.userAgent;
      $.cookie('browserId', browserId);
    }

 /*if ( navigator.geolocation) {
        var watchId = navigator.geolocation.watchPosition(function(position) {  
          map.setCenter({ lat: position.coords.latitude, lng: position.coords.longitude});
        });
      } else {

      }*/

      //var socket = io.connect('http://geo.chr.io/');
      var socket = io.connect('http://geo.brl.io/');
      socket.on('connect', function(){
        console.log('connected');

        $('#statusb').addClass('icon-ok-circle');
        $('#statusb').removeClass('icon-ban-circle');

        socket.emit('hello', {
            tag: $('#tag').val(),
            id: browserId
        });

      });
      socket.on('disconnect', function(){
        console.log('disconnected');
        $('#statusb').removeClass('icon-ok-circle');
        $('#statusb').addClass('icon-ban-circle');
        //alert('disconnected');
      });
      
      var markers = {};



      var drawMarkers = function(){
        //map.removeMarkers();

        for (var key in markers) {
          var m = markers[key];
          map.addMarker(m);
        }
      };

      socket.on('view', function (data) {

        console.log(data);

        if (typeof markers[data.id] === 'undefined') {

          markers[data.id] = {
            lat: data.position.lat,
            lng: data.position.lng,
            title: data.name,
            details: {
              tag: data.tag,
              id: data.id,
              date: data.date,
              accuracy: data.accuracy
            },
            click: function(e){
              alert(e.details.name);
            }
          };
          drawMarkers();
        } else {
          markers[data.id].lat = data.position.latitude;
          markers[data.id].lng = data.position.longitude;
        }

        console.log(data);
      });


          socket.on('connect', function(){
            console.log('connected');
            document.getElementById('status').innerHTML = 'connected';

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position){
                    map.setCenter(position.coords.latitude, position.coords.longitude, 13);
                }, function(error){
                    document.getElementById('status').innerHTML = typeof msg == 'string' ? msg : "error";
                });

                var watchId = navigator.geolocation.watchPosition(function(position) {  
                    console.log('send location');
                    socket.emit('position', {
                        position: {
                            lng: position.coords.longitude,
                            lat: position.coords.latitude
                        },
                        date: position.timestamp,
                        accuracy: position.coords.accuracy,
                        tag: document.getElementById('tag').value,
                        name: document.getElementById('name').value,
                        id: browserId
                    });
                });
            } else {
                document.getElementById('status').innerHTML = 'geolocation not available';
            }

          });

          socket.on('disconnect', function(){
            console.log('disconnected');
            navigator.geolocation.clearWatch(watchId);
            document.getElementById('status').innerHTML = 'disconnected';
          });

          socket.on('news', function (data) {
            console.log(data);
            socket.emit('my other event', { my: 'data' });
          });




});
